#!/bin/sh
# conf.sh
PASSWORD=sysprog2019

read port
echo "[Left Click Mouse] to bind or [Right Click Mouse] to unbind"
result=$(echo $PASSWORD | sudo python3 scripts/read_mouse.py | tee /dev/stderr)
case $result in
    "Left Button Clicked") echo $PASSWORD | echo $port | sudo tee /sys/bus/usb/drivers/usb/bind;;
    "Right Button Clicked") echo $PASSWORD | echo $port | sudo tee /sys/bus/usb/drivers/usb/unbind;;
esac
