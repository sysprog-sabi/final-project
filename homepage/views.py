import os
import subprocess
from django.shortcuts import render,redirect

# Create your views here.
def index(request):
    output = os.popen('lsusb').read()

    return render(request, 'index.html', {'output': output})

def bind(request):
   if(request.method == "POST"):
        port = request.POST.get("port")
        subprocess.run(["./scripts/tk.sh"], universal_newlines=True, input=port)
        return redirect("/")