Group   : SaBi  
Members : 
1. Nabila Ayu Dewanti (1806205312)
2. Salsabila Adnan (1806186704)

## Project Description
This is a final project submission for Systems Programming class fall term 2020. Our app have a function to bind or unbind usb devices by the click of a mouse. This project integrates the use of **psmouse** and **usb** drivers.

## Prerequisites
Before starting, make sure
 - Virtualbox network attached to Bridged Adapter
 - Start and login to your VirtualBox
 - Clone this repository
 - Activate virtual environment
 - Install all requirements
 - Migrate the project
 - Change tk.sh permissions with `chmod +x scripts/tk.sh`
 - Find your corresponding server ip with `ifconfig`

## How to Run

 1. Run this command to start 
	```
	python manage.py runserver your_server_ip:8000
	```
 2. Visit your server’s IP address followed by :8000 in your web browser 	`http://your_server_ip:8000` 
 3. Input port usb you want to bind/unbind in input field
 4. Click button "Bind/Unbind"
 5. Back to your VirtualBox
 6. Left Click Mouse to bind or Right Click Mouse to unbind in your VirtualBox (make sure to capture your mouse first)
 7. Enter user password
 8. Note: when binding, you need to refresh the page manually to see the updated usb list

## To Automatically Start
Add command lines at the end of **/etc/profile** to activate your virtual envronment and run your server (our example could be seen in **/scripts/profile**).

## To Compile Custom Kernel
Load our custom configuration in **/scripts/config-4.15.1deb-sabi-version**.
